# react-and-spring-data-rest

The application has a react frontend and a Spring Boot Rest API, packaged as a single module Maven application.

You can build the application running (`./mvnw clean verify`), that will generate a Spring Boot flat JAR in the target folder.

To start the application you can just run (`java -jar target/react-and-spring-data-rest-*.jar`), then you can call the API by using the following curl (shown with its output):

---

\$ curl -v -u greg:turnquist localhost:8080/api/employees/1
{
"firstName" : "Frodo",
"lastName" : "Baggins",
"description" : "ring bearer",
"manager" : {
"name" : "greg",
"roles" : [ "ROLE_MANAGER" ]
},
"\_links" : {
"self" : {
"href" : "http://localhost:8080/api/employees/1"
}
}
}

---

To see the frontend, navigate to http://localhost:8080. You are immediately redirected to a login form. Log in as `greg/turnquist`


######################################################################################################

# DevOps + Cloud

## Overvierw
This project to demonstrate:
- Building a react frontend and a Spring Boot Rest API
- Building Docker image and Publish
- Creating CICD pipelines with Gitlab
- Creatation of Infrastactire with terraform
- Automated build & Deployment using Gitlab CI/CD
- Deployments in Azure AKS in multiple namespaces for each environment(Dev, Prod)

 Below components are used for CI/CD and IAC:

##### CICD: 
- Gitlab Repository
- Gitlab CI/CD Pipelines
- Gitlab Container Registry
    
##### IAC:
- Terraform
- Docker

##### Cloud:
- Azure AKS
- Ingress
- Azure RG
- Azure ACR

CICD components:

![picture](/img/tools.png)

## Prerequisites:
Below are prerequisites which should be there before running terraform locally

- Azure account with subscription, then you can login with
    ```console
    az login
    ```

- Then Check Terraform version
    ```console
    terraform -v
    ```
If terrraform is not exist in you machine, Try to install using this [link](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/azure-get-started)


## Getting Started
### Creating Infrastracture with Terraform
    
![picture](/img/terraform.png)

- Clone source code from gitlab repository
    ```console
    git clone https://gitlab.com/nareshreddybogathi/react-and-spring-cicd.git
    cd react-and-spring-cicd
    ```
- Login to your AZ account for creating Infrastracture.

    ```console
    az login
    ```
    You will be redirected to do your azure login and then to be able to work on the terminal. If everything is ok, you should see the following:
    ![picture](/img/az_login.png)

- Go into the `terraform-aks-k8s` directory in source code and run `terraform init`. This will initialize the project, which downloads a plugin that allows Terraform to interact with our project.

    ```console
    cd terraform-aks-k8s
    terraform init
    ```

    ![picture](/img/terraform_init.png)

- Terraform Plans are usually run to `validate configuration changes` and confirm that the resulting actions are as expected.
    ```console
    terraform plan
    ```
    ![picture](/img/terraform_plan_2.png) 
    ![picture](/img/terraform_plan_1.png)

- Provision all resources which requireds for this project(AKS, RG, Ingress, PublicIP).

    ```console
    terraform apply
    ```
    When Terraform asks you to confirm type `yes` and press ENTER.
    ![picture](/img/terraform_apply_1.png) 

    After provisioning successfully, we can see all terraform outputs here
    ![picture](/img/terraform_apply_2.png)


- After successful terraform execution, we can see list of clusters by using following cmd:
    ```console
    az aks list --out table
    ```
    ![picture](/img/aks_list.png)

    Copy `Your_Cluster_Name`,  `Your_ResourceGroup` and replace that in below cli
    ```console
    az aks get-credentials --resource-group Your_ResourceGroup --name Your_Cluster_Name --admin
    kubectl get nodes
    kubectl get namespaces
    ```




## Steps to Integrate AKS + Gitlab 
If you are creating new cluster, then follow this below steps(optional)

- Login to azure in your terminal
    ```console
    az login
    ```

- Please note that you should change YOUR_RESOURCE_GROUP_HERE & THE_NAME_OF_YOUR_NEW_CLUSTER to the one you created previous step from above the terraform --outputs. 
    ```console
    az aks get-credentials -n THE_NAME_OF_YOUR_NEW_CLUSTER -g YOUR_RESOURCE_GROUP_HERE
    ```

    It will merge and allow you to use the created cluster.

- Now we need to get your secret keys
    ```console
    kubectl get secrets
    ```

    Copy the one that appear for you that might be like: default-token-ccZZ9
    With the name copied, run
    ```console
    kubectl get secrets default-token-ccZZ9 -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
    ```

    Remember that you need to change my fake default-token-ccZZ9 to the one you recived fron previous cmd output: `kubectl get secrets`

    It will appear to you something like the following`CA Certificate`:
    ```console
    -----BEGIN CERTIFICATE-----
    FAJidabf23698gAwIBAgIQbF5jlFxYUX3WEdEX+ryJNTANBgkqhkiG9w0BAQsFADAN
    MQs...
    -----END CERTIFICATE-----
    ```
    Copy everything and we will use later on.


- Go into the `Project` directory in source code. At this folder run following commands to get permissions to gitlab

    ```console
    kubectl apply -f deployment/gitlab-connector/gitlab-admin-service-account.yaml
    ```

    And then:
    ```console
    kubectl apply -f deployment/gitlab-connector/gitlab-admin-cluster-role-binding.yaml
    ```

    With the service created, we are now able to collect the token that Gitlab will ask. Copy all the content of the token that in this case is shorten as this:
    ```console
    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
    ```
    Copy all the content of the token that in this case is shorten as this `Service Token`:
    ```console
    eyJhbG...t47cbe7opgW5dB1gnW7s9aF_WDdKE27nYWSmtZwWT_XlZUynvvifpQtlSu5R-rm1pHC-81oDQ0
    ```


- Now we will need the created endpoint from your new cluster that can be found `API URL` [here](https://portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.ContainerService%2FmanagedClusters)

    Copy API server address from your az cluster


- Now you have all details to intergrate aks cluster in gitlab
    1. Cluster_Name
    2. API URL
    3. CA Certificate
    4. Service Token
    5. Namespace prefix (Same name as we provided in gitlab-ci file `PROJECT_NAME:`)

- Open you gitlab and click on your repo kubernetes clusters in [infrastructure](https://gitlab.com/nareshreddybogathi/react-and-spring-cicd/-/clusters)

![picture](/img/gitlabbasic_kubernetes_configuration.png)

- For connecting private gitlab registry on aks cluster we should run this cmd, This will create a token at aks cluster with the gitlab creadentials that you have. For public repo it's (optional) (This is mainlly for pull docker images from gitlab continer registry)
    ```console
    kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=YOUR_USER_NAME_GOES_HERE --docker-password=YOUR_PASSWORD_HERE
    ```


## Automated build & Deployment using Gitlab CI/CD + AKS

The project consists of two Environment deployments in end(Dev and Prod) in two differant namespaces. So, I have used one pipeline for both environments. Pipeline consists of three stages (Build, Docker, K8s) – first one to clone source and run Maven build, docker build and publish and last one consists of Dev and prod deployment stages for Azure AKS. and Prod deploy is a manual approvel step. [source code](https://gitlab.com/nareshreddybogathi/react-and-spring-cicd)
    ![picture](/img/pipeline.png)

- Here we can check all previous build and deploy [history](https://gitlab.com/nareshreddybogathi/react-and-spring-cicd/-/pipelines):
 
    ![picture](/img/pipeline_history.png)

- Here we can see all list of images build though Gitlab CI/CD, Same Docker images are used in AKS deployment. This images are storing with `$CI_COMMIT_SHORT_SHA` for each buld run it will create a new image based on gitlab commit id. [link](https://gitlab.com/nareshreddybogathi/react-and-spring-cicd/container_registry/2087253)

    ![picture](/img/images.png)


- In same gitlab we can monitor all k8s deployment metrics and logs for each service and environments.

    ![picture](/img/monitor.png) 
    ![picture](/img/monitor-2.png)



- Once deployment is successful in Gitlab we can see all deployment and service details in your terminal with using the below commands:

    ##### - Development

    ```console
    kubectl get deployments -n reactspringcicd-development
    kubectl get svc -n reactspringcicd-development
    kubectl get pods -n reactspringcicd-development
    ```
    ![picture](/img/dev.png)

    ##### - Production

    ```console
    kubectl get deployments -n reactspringcicd-production
    kubectl get svc -n reactspringcicd-production
    kubectl get pods -n reactspringcicd-production
    ```
    ![picture](/img/prod.png)


- Finally!! we can access website with Ingress publicIP.

    Below diagram shows the flow between the components.

    ![picture](/img/123.png)

    - ##### Dev: 
    Run the below command to get the public IP adreess, which can be used for accessing the website in brower as shown below 
    ```console
    kubectl get svc -n reactspringcicd-development
    ```
    Link: http://20.105.1.21/login

    ![picture](/img/dev-web.png)

    - ##### Prod:
    Run the below command to get the public IP adreess, which can be used for accessing the website in brower as shown below
    ```console
    kubectl get svc -n reactspringcicd-production
    ```
    Link: http://20.82.168.251/login

    ![picture](/img/prod-web.png) ![picture](/img/login.png)

    
- Micro-services monitoring with `Grafana` + `Prometeus`

   - [Prometeus](http://20.82.216.146/graph?g0.expr=apiserver_request_total&g0.tab=0&g0.stacked=0&g0.range_input=1h)
   - [Grafana](http://20.82.217.158/d/P9_9dqk7z/kubernetes-cluster-monitoring-via-prometheus?orgId=1&refresh=10s)

    ![picture](/img/grafana.png)

## Remove this demo resources 
If you want to remove the infrastracture follow the steps:

Go into the `terraform-aks-k8s` directory in source code and run `terraform destroy`. To confirm deletion please enter `yes`.

![picture](/img/destroy.png)

