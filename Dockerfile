# We don't want to start from scratch.

#
# Package stage
#
FROM openjdk:11-jre-slim

# Create an application directory
RUN mkdir -p /backend

# The /backend directory should act as the main application directory
WORKDIR /backend

# Copy or project directory (locally) in the current directory of our docker image (/app)
COPY /target/react-and-spring-data-rest-*.jar ./react-and-spring-data-rest.jar

# Expose $PORT on container.
EXPOSE 8080

# Start the app
ENTRYPOINT ["java","-jar","./react-and-spring-data-rest.jar"]
