provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "default" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_kubernetes_cluster" "default" {
  name                = var.cluster_name
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  dns_prefix          = var.dns_prefix

  default_node_pool {
    name                = "default"
    node_count          = var.node_count
    vm_size             = "Standard_D2_v2"
    enable_auto_scaling = false #If we set true it will allow infrastructure to be able to scale automatically.
    # min_count           = 1
    # max_count           = 2
    # os_disk_size_gb     = 30
    # type                = "VirtualMachineScaleSets"
  }

  identity {
    type = "SystemAssigned"
  }
  # service_principal {
  #   client_id     = var.appId
  #   client_secret = var.password
  # }

  role_based_access_control {
    enabled = true
  }

  addon_profile {
    kube_dashboard {
      enabled = false
    }
  }

  tags = {
    Environment = "Development"
    Creator     = "Terraform"
  }
}
