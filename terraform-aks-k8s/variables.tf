variable "node_count" {
  default = 2
}

variable "dns_prefix" {
  default = "reactspringcicd"
}

variable "cluster_name" {
  default = "reactspringcicd"
}

variable "resource_group_name" {
  default = "azure-reactspringcicd"
}

variable "location" {
  default = "North Europe"
}

variable "databases_name" {
  description = "Names of the databases to create for this server"
  type        = string
  default     = "reactspringcicdsqldatabase"
}

