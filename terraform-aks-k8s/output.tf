output "resource_group_name" {
  value = azurerm_resource_group.default.name
}

output "kubernetes_cluster_name" {
  value = azurerm_kubernetes_cluster.default.name
}

output "get_credentials" {
  value = "az aks get-credentials --name ${var.cluster_name} --resource-group ${var.resource_group_name} --admin"
}

output "ingress-ip" {
  value = azurerm_public_ip.ingress.ip_address
}

output "sql_server_fqdn" {
  description = "Fully qualified domain name of the SQL Server"
  value       = azurerm_sql_server.default.fully_qualified_domain_name
}

output "azurerm_sql_database" {
  description = "SQL Databse name"
  value       = azurerm_sql_database.default.name
}
