resource "azurerm_sql_server" "default" {
  name                         = "reactspringcicdsqlserver"
  resource_group_name          = azurerm_resource_group.default.name
  location                     = azurerm_resource_group.default.location
  version                      = "12.0"
  administrator_login          = "YWRtaW4="
  administrator_login_password = "YWRtaW4xMjM="

  tags = {
    environment = "production"
  }
}

resource "azurerm_storage_account" "default" {
  name                     = "reactspringcicdsa"
  resource_group_name      = azurerm_resource_group.default.name
  location                 = azurerm_resource_group.default.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_sql_database" "default" {
  name                = var.databases_name
  resource_group_name = azurerm_resource_group.default.name
  location            = azurerm_resource_group.default.location
  server_name         = azurerm_sql_server.default.name

  extended_auditing_policy {
    storage_endpoint                        = azurerm_storage_account.default.primary_blob_endpoint
    storage_account_access_key              = azurerm_storage_account.default.primary_access_key
    storage_account_access_key_is_secondary = true
    retention_in_days                       = 6
  }



  tags = {
    environment = "production"
  }
}
