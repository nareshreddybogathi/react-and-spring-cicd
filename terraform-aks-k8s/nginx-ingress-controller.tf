provider "helm" {
  kubernetes {
    host                   = azurerm_kubernetes_cluster.default.kube_config.0.host
    client_certificate     = base64decode(azurerm_kubernetes_cluster.default.kube_config.0.client_certificate)
    client_key             = base64decode(azurerm_kubernetes_cluster.default.kube_config.0.client_key)
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.default.kube_config.0.cluster_ca_certificate)
  }
}

# Create Static Public IP Address to be used by Nginx Ingress
resource "azurerm_public_ip" "ingress" {
  name                = "nginx-ingress-controller-pip"
  resource_group_name = azurerm_resource_group.default.name
  location            = azurerm_resource_group.default.location
  allocation_method   = "Static"
  domain_name_label   = var.dns_prefix
}

# Install Nginx Ingress using Helm Chart
resource "helm_release" "nginx-ingress" {
  name             = "nginx-ingress-controller"
  namespace        = "ingress"
  create_namespace = true
  repository       = "https://charts.bitnami.com/bitnami" # Add Kubernetes Stable Helm charts repo
  chart            = "nginx-ingress-controller"

  set {
    name  = "rbac.create"
    value = "true"
  }

  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }
  set {
    name  = "controller.service.loadBalancerIP"
    value = azurerm_public_ip.ingress.ip_address
  }
}
