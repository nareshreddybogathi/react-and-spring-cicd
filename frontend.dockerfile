# We don't want to start from scratch.
# That is why we tell node here to use the current node image as base.
FROM node:latest

# Create an application directory
RUN mkdir -p /frontend

# The /app directory should act as the main application directory
WORKDIR /frontend

# Copy the app package and package-lock.json file
COPY package*.json /frontend/

# Install node packages
RUN npm install -g npm@7.19.1

# Copy or project directory (locally) in the current directory of our docker image (/app)
COPY . /frontend

# Expose $PORT on container.
# We use a varibale here as the port is something that can differ on the environment.
EXPOSE 8080

# Start the app
ENTRYPOINT [ "npm", "start" ]
